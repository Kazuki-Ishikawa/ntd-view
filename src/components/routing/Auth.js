import React from 'react';
import { Redirect } from 'react-router-dom';
import {getSession} from '../common/Util.js';


const errorMessage="ログインしてください。";

const Auth = props =>
  !(getSession() === null || getSession() === "")
  ? props.children 
  : <Redirect to={{
    pathname: "/login",
    // search:"error",
    state: { errorMessage: errorMessage }
  }}/> ;


export default Auth;