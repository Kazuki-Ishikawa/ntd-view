import React, { Component } from 'react';
import { BrowserRouter, Route,Switch} from 'react-router-dom';

import Setting from '../setting/Setting';
import Board from '../board/Board';
import Login from '../login/Login.js';
import Signup from '../signup/Signup.js';

import Auth from './Auth.js';


class Router extends Component {
  render(){
    return(
      <BrowserRouter>
        <Switch>
          <Route exact path='/login' component={Login} />
          <Route exact path='/signup' component={Signup} />
          
          <Auth>
            <Switch>
              <Route exact path='/setting' component={Setting} />
              <Route exact path='/board' component={Board} />
              {/* <Route exact path='/getDetail/:cd' component={GetDetail} /> */}
            </Switch>
          </Auth>       
        </Switch>
      </BrowserRouter>
    )
  }
}
export default Router;