import React, { useState, useEffect } from 'react';
import '@atlaskit/css-reset';
import { DragDropContext, Droppable } from 'react-beautiful-dnd';
import styled from 'styled-components';
import { useForm } from "react-hook-form";
import axios from 'axios';
import ClickAwayListener from "react-click-away-listener";
import _ from "lodash/fp";

import { getRawId, logout, isCloseDeadline,formatStrToObj} from '../common/Util';
import Copyright from '../common/Copyright';
import TutorialPopup from "./TutorialPopup.js";
import List from './List';
import { updateList, updateTask } from '../apis/PutApi';
import { postNewList } from "../apis/PostApi"

const Container = styled.div`
  display:flex;
`

const Board = (props) => {
  const { register, handleSubmit, watch, errors } = useForm();

  const grid = 8;

  const getListStyle = isDraggingOver => ({
    background: isDraggingOver ? '#eeeeee' : '#eeefee',

    display: 'flex',
    padding: grid,
    // overflow: 'hidden',
    width: "auto",
    "minHeight": "100vh",
    height: "auto",
  });

  const [state, setState] = useState(
    {
      tasks: {},
      lists: {},
      listOrder: []
    }
  );

  // const [rawData, setRawData] = useState([]);
  // const [tasks, setTasks] = useState([]);
  //  const [putDetail, setPutDetail] = useState(false);
  const [isPosted, setIsPosted] = useState(false);
  const [listData, setListData] = useState([]);
  const [isListPosted, setIsListPosted] = useState(false);
  const [isListDeleted, setIsListDeleted] = useState(false);
  const [recentBoardId, setRecentBoardId] = useState();
  const [addListForm, showAddListForm] = useState(false);
  const [isDeleted, setIsDeleted] = useState(false);
  const [isDetailPosted, setIsDetailPosted] = useState(false);
  const [editListTitle, doneEditListTitle] = useState(false);

  const [listDraged, setListDraged] = useState(false);
  const [taskDraged, setTaskDraged] = useState(false);


  useEffect(() => {

    //when someone is the first signup
    if (props.location.state !== undefined) {
      console.log("is Show Tutorial : " + props.location.state.isShowTutorial)
    }

    // when the component called at first
    if (recentBoardId === undefined) {

      //const boardId = getRecentBoardOfLoginUser()
      // setRecentBoardId(boardId);
      /*
      const aa = async () => {
        await getRecentBoardOfLoginUser();
      }
      aa();
      */
      getRecentBoardOfLoginUser();
    } else {
      console.log("Board Mounting!")
      console.log(recentBoardId);

      const aa = async () => {
        await getRawDataByBoardId(recentBoardId);
      }
        ;
      aa();
      ;
      //await getRawDataByBoardId(recentBoardId);
      //getRawDataByBoardId(recentBoardId);
    }

    return () => {
      console.log("Board Unmounting")
    }
  }, [isPosted, recentBoardId, editListTitle,
    isListPosted, isListDeleted, isDeleted,
    isDetailPosted, listDraged, taskDraged
  ]);


  useEffect(() => {
    console.log("List Show task form Mounting!")
    console.log(addListForm);

    //when open a task form, command to focus a task form
    addListForm && document.getElementById("add-list-input").focus();
    return () => {
      console.log("List show task form Unmounting")
    }
  }, [addListForm]);


  const getRawDataByBoardId = (boardId) => {
    return axios.get(process.env.REACT_APP_API_URI + "list/get/byBoardId/" + boardId).then((res) => {
      const data = res.data;
      console.log("raw data:");
      console.log(data);
      console.log("tasks :");

      console.log(data.map((index) =>
        index.tasks.map((index) => index)
      ));

      //apply第二引数に二次元配列を加えると一次元配列に変換
      const taskArr = Array.prototype.concat.apply([],
        data.map((index) =>
          index.tasks.map((index) => index))//{array(10),array(3),array(2)....}
      );

      console.log("taskArr : ")
      console.log(taskArr)

      //generate an initial data here
      let BoardState = {
        tasks: {},
        lists: {},
        listOrder: []
      };

      //taskArr[{},{},{},{},{},.....]
      taskArr.forEach(element => {
        let idStr = element.id.toString(10);

        element.id = "task-" + element.id.toString(10);//convert '1' to 'task-1'
        element["dueColor"] = isCloseDeadline(element.dueDate);
        element["dueDateObj"] = formatStrToObj(element.dueDate);
        BoardState.tasks["task-" + idStr] = element; //object にtaskを追加
      });

      data.forEach(element => {
        console.log("Call rawData :" + element.id);
        let idStr = element.id.toString(10);
        element.id = "list-" + element.id.toString(10);
        element.taskIds = element.tasks.map(index => index.id);
        BoardState.lists["list-" + idStr] = element;
        BoardState.listOrder.push("list-" + idStr);
      });

      console.log("BoardState");
      console.log(BoardState);

      console.log("state initial");
      console.log(state);

      setState(BoardState);

      console.log("listData")
      console.log(data.map(element => element))
      setListData(data.map(element => element))

    });
  }

  const onSubmit = (data) => {
    console.log("onSubmit: ");
    console.log(data)
    const listName = watch("list");
    const newList = {
      "name": listName,
      "boardId": recentBoardId,
      "sequenceNum": listData.length
    };

    console.log("new list :");
    console.log(newList);


    // Warning : Asynchronous communication

    postNewList(newList).then(res => {
      setIsListPosted(!isListPosted);
      showAddListForm(!addListForm);

    });


    // (async () => {
    //   const postPromise = postNewList(newList);
    //   setIsListPosted(!isListPosted);
    //   showAddListForm(!addListForm);
    //   await postPromise;
    // })()


  }

  const loginUserId = sessionStorage.getItem('loginUserId');

  const getRecentBoardOfLoginUser = async () => {
    //ログインしてるユーザーが所有するBoardIdの参照

    const res = await axios.get(process.env.REACT_APP_API_URI + "board/get/byUserId/" + loginUserId)
    setRecentBoardId(res.data[0].id);
    //return res.data[0].id; //降順にデータを取得しているので0要素目が最新
  }
  /*
    const postNewList = (newList) => {

      axios.post(process.env.REACT_APP_API_URI + "list/set", newList).then((res) => {
        console.log('Post List')
        console.log(res.data)
        setIsListPosted(!isListPosted);
        showAddListForm(!addListForm);
      });

    }
  */

  /*
    const getRawId = (strId) => {
      const rawId = parseInt(strId.replace(/^list-|task-/, ''));
      return rawId;
    }
  */
  //the method is about drag and drop
  const onDragEnd = result => {
    const { destination, source, draggableId } = result

    // dropped outside the list
    if (!result.destination) {
      return;
    }

    if (
      destination.droppableId === source.droppableId &&
      destination.index === source.index
    ) {
      return;
    }


    if (result.type === "LIST") {

      const newListOrder = Array.from(state.listOrder);
      newListOrder.splice(source.index, 1)
      newListOrder.splice(destination.index, 0, draggableId)

      const newState = {
        ...state,
        listOrder: newListOrder
      }


      setState(newState)

      /* update list's sequenceNum*/

      let start;//start index
      let end;//end index
      // moving list from right to left
      if (source.index - destination.index > 0) {
        start = destination.index
        end = source.index
      } else {
        start = source.index
        end = destination.index
      }

      (async () => {

        let updataPromises = [];
        for (let i = start; i <= end; i++) {
          let listId = newListOrder[i];
          let newList = {
            ...state.lists[listId],
            sequenceNum: i
          }
          console.log("new List after list Dropped")
          console.log(newList)

          //let newListId = listId.replace(/list-/g, "");
          let updataPromise = updateList(getRawId(listId), newList);
          updataPromises.push(updataPromise);
        }

        await Promise.all(updataPromises);
        setListDraged(!listDraged);
        return;
      })();

      return;
    }



    //the following is about dropped one of tasks

    const start = state.lists[source.droppableId]
    const finish = state.lists[destination.droppableId]

    if (start === finish) {

      const newTaskIds = Array.from(start.taskIds)
      newTaskIds.splice(source.index, 1)
      newTaskIds.splice(destination.index, 0, draggableId)

      console.log("*******************************");
      console.log("Moving from one list to the same list");

      console.log("draggableId");
      console.log(draggableId);

      console.log("destination");
      console.log(destination);

      console.log("source");
      console.log(source);


      const newList = {
        ...start,
        taskIds: newTaskIds
      }

      const newState = {
        ...state,
        lists: {
          ...state.lists,
          [newList.id]: newList
        }
      }

      setState(newState)

      let startIndex;//start index
      let endIndex;//end index
      // moving task from bottum to top
      if (source.index - destination.index > 0) {
        startIndex = destination.index
        endIndex = source.index
      } else {
        startIndex = source.index
        endIndex = destination.index
      }

      (async () => {

        let updataPromises = [];
        for (let i = startIndex; i <= endIndex; i++) {
          let taskId = newTaskIds[i];
          let newTask = {
            ...state.tasks[taskId],
            "sequenceNum": i
          }
          console.log("new Task after list Dropped | index : sequenceNum -> " + i + " : " + newTask.sequenceNum);
          console.log(newTask)
          let updataPromise = updateTask(getRawId(taskId), newTask);

          updataPromises.push(updataPromise);

        }
        await Promise.all(updataPromises);
        setTaskDraged(!taskDraged);
        return;
      })();

      return
    }

    // Moving from one list to another
    const startTaskIds = Array.from(start.taskIds)
    startTaskIds.splice(source.index, 1)
    const newStart = {
      ...start,
      taskIds: startTaskIds
    }

    const finishTaskIds = Array.from(finish.taskIds)
    finishTaskIds.splice(destination.index, 0, draggableId)
    const newFinish = {
      ...finish,
      taskIds: finishTaskIds
    }
    console.log("*******************************");
    console.log("Moving from one list to another");

    console.log("source")
    console.log(source)
    console.log("destination")
    console.log(destination);

    console.log("draggableId");
    console.log(draggableId);

    console.log("start");
    console.log(start);
    console.log("finish");
    console.log(finish);

    console.log("newStart");
    console.log(newStart);
    console.log("newFinish");
    console.log(newFinish);
    console.log(state);

    const newState = {
      ...state,
      lists: {
        ...state.lists,
        [newStart.id]: newStart,
        [newFinish.id]: newFinish
      }
    }
    setState(newState)
    console.log("new state :");
    console.log(newState);

    const newListIdInt = getRawId(newFinish.id);
    console.log("newFinish list id");
    console.log(newListIdInt);

    const taskIdInt = getRawId(draggableId);
    console.log("new task id  Draggable ID");
    console.log(taskIdInt);

    console.log(newState.tasks[draggableId]);
    const draggedTask = newState.tasks[draggableId];

    console.log(draggedTask);


    //let endIndex = destination.index//end index
    // moving list from right to left



    (async () => {

      let startIndex = source.index;
      let updataPromises = [];
      for (let i = startIndex; i < startTaskIds.length; i++) {
        let taskId = startTaskIds[i];
        let newTask = {
          ...state.tasks[taskId],
          "sequenceNum": i
        }
        console.log("new Task after list Dropped | index : sequenceNum -> " + i + " : " + newTask.sequenceNum);
        console.log(newTask)

        let updataTaskPromise = updateTask(getRawId(taskId), newTask)
        updataPromises.push(updataTaskPromise)
      }

      startIndex = destination.index

      for (let i = startIndex; i < finishTaskIds.length; i++) {
        let taskId = finishTaskIds[i];
        let newTask = {
          ...state.tasks[taskId],
          "listId": newListIdInt,
          "sequenceNum": i
        }
        console.log("new Task after list Dropped | index : sequenceNum -> " + i + " : " + newTask.sequenceNum);
        console.log(newTask)
        let updataPromise = updateTask(getRawId(taskId), newTask);
        updataPromises.push(updataPromise);
      }
      console.log("updataPromises and updataTaskPromise")
      console.log(updataPromises);

      await Promise.all(updataPromises);
      console.log("set task draged " + taskDraged);
      setTaskDraged(!taskDraged);

      return;
    })();

    //    recreateTasks();

  }

  const finishedTutorial = () => {
    props.location.state.isShowTutorial = false;
    props.history.push('./board');
    console.log("フラグ反転")
  }

  return (

    <div>

      <div className="fixed-header">
        <div className="header-items">
          <div className="header-icon">
            <a href="./board" id="header-icon"><span id="fake">FAKE</span>Trello</a>
          </div>


          <div className="header-item">
            <span onClick={() => logout(() => props.history.push('./login'))}
              test={"test"} className="header-text">log out</span>
          </div>
          <div className="header-item">
            <a href="./setting" className="header-text">setting</a>
          </div>

        </div>
      </div>


      <div className="home-back">

        {props.location.state !== undefined
          ? props.location.state.isShowTutorial === true
            ? <TutorialPopup
              tutorialFlag={props.location.state.isShowTutorial}
              finishedTutorial={finishedTutorial}
            />
            : null
          : null
        }
        {/* <TutorialPopup tutorialFlag={props.location.state.isShowTutorial}/> */}

        <DragDropContext onDragEnd={onDragEnd}>

          <Container>
            {/* <div>{loginUserId}</div> */}

            <div className="list-wrapper">
              <div className="add-list">
                <ClickAwayListener onClickAway={() => showAddListForm(false)}>
                  <div className="add-list-btn" onClick={() => showAddListForm(!addListForm)}>
                    <div className="add-list-title">add List</div>
                  </div>
                  {_.get("list.type", errors) === "required" && (<span className="list-add-error">入力必須です</span>)}
                  {_.get("list.type", errors) === "maxLength" && (<span className="list-add-error">文字数30以下でお願いします</span>)}
                  {addListForm && (
                    <form className="add-list-form addListForm" onSubmit={handleSubmit(onSubmit)}>
                      <input id="add-list-input" name="list" type="text" initialvalue=""
                        placeholder="リスト名"
                        ref={register({ required: true, maxLength: 30 })}
                      ></input>
                      <input type="submit" id="add-list-submit" value="add"></input>
                    </form>
                  )}
                </ClickAwayListener>
              </div>


              <Droppable droppableId="droppable" type="LIST" direction="horizontal">
                {(provided, snapshot) => (
                  <div
                    ref={provided.innerRef}
                    style={getListStyle(snapshot.isDraggingOver)}
                    {...provided.droppableProps}
                  >

                    {state.listOrder.map((listId, index) => {
                      const list = state.lists[listId]
                      const tasks = list.taskIds.map(
                        taskId => state.tasks[taskId]
                      )

                      return (
                        <List key={list.id} list={list} tasks={tasks} listId={listId} listData={listData} index={index}
                          boardId={recentBoardId} setIsPosted={setIsPosted} isPosted={isPosted}
                          isListDeleted={isListDeleted} setIsListDeleted={setIsListDeleted}
                          isDeleted={isDeleted} setIsDeleted={setIsDeleted}
                          setIsDetailPosted={setIsDetailPosted}
                          isDetailPosted={isDetailPosted}
                          editListTitle={editListTitle} doneEditListTitle={doneEditListTitle}
                        />
                      )
                    })}
                    {provided.placeholder}
                  </div>
                )}

              </Droppable>
            </div>
          </Container>

        </DragDropContext>

      </div>
      <div style={{ position: 'relative', marginTop: '20px', background: '#e6e6e6' }}><Copyright /></div>
    </div>
  )
}

export default Board;
