import React, { useEffect, useState } from 'react';
import Popup from 'reactjs-popup';
import Carousel from 'nuka-carousel';
import tutorial1 from '../../img/tutorial1.png';
import tutorial2 from '../../img/tutorial2.gif';
import tutorial3 from '../../img/tutorial3.gif';
import tutorial4 from '../../img/tutorial4.gif';
import tutorial5 from '../../img/tutorial5.gif';
import tutorial6 from '../../img/tutorial6.gif';
import tutorial7 from '../../img/tutorial7.gif';
import "../../CSS/Popup.css";

const TutorialPopup = (props) => {

  const [isFinishedTutorial,setIsFinishedTutorial] = useState(false);


  useEffect(() => {
    if(isFinishedTutorial === true){
      console.log("Tutorial Mounting!")
      console.log(props);
      props.finishedTutorial();

    }

    return () => {
      console.log("List Unmounting")
    }
  }, [isFinishedTutorial]);


  return (
    <Popup defaultOpen modal="true" parrent={props}>
      {(close, props) => (
        <div className="tutorial-popup-back">
          <div className="tutorial-popup-wrapper">
            <Carousel >
            <img src={tutorial1} alt=""/>
            <img src={tutorial2} alt=""/>
            <img src={tutorial3} alt=""/>
            <img src={tutorial4} alt=""/>
            <img src={tutorial5} alt=""/>
            <img src={tutorial6} alt=""/>
            <img className = "lastSlide" src={tutorial7} alt=""/>
            <div className="actions">
              <h3 className="popup-title">これでチュートリアルは終わりです</h3>
              <p className="popup-title2">「continue」を押して、あなたのボードを開きましょう！</p>
              <button
                className="popup-submit-button"
                onClick={(props) => {
                  setIsFinishedTutorial(!isFinishedTutorial);

                  close();
                }}
              >
                continue
          </button>
            </div>
            </Carousel>

          </div>
        </div>
      )}
    </Popup>
  )

}
export default TutorialPopup;