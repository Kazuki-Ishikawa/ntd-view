import React, {  useState, useEffect } from 'react';

import { Droppable,Draggable } from 'react-beautiful-dnd';
import { useForm } from "react-hook-form";
import ClickAwayListener from "react-click-away-listener";
import _ from "lodash/fp";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import Task from './Task';
import {getRawId, getSession} from "../common/Util";
import {updateList} from "../apis/PutApi";
import {deleteList} from "../apis/DeleteApi";
import {postNewTask} from "../apis/PostApi";

import "../../CSS/Home.css";
import {Container,getItemStyle,Title,ToggleButton,TaskList} from "../style/StyledList";


const List = (props) => {
  const { register, handleSubmit, watch, errors } = useForm();

  const [isShowTaskForm, toggleIsShowTaskForm] = useState(false);
  const [isTitleEdit, toggleTitleEdit]= useState(true);

  //FormにPostする情報

  let [task, setTask] = useState({
    "task": "API TEST FROM REACT",
    "userId": getSession(),
    "listId": 1
  });


  useEffect(() => {
    console.log("List Show task form Mounting!")
    console.log(isShowTaskForm);

    //when open a task form, command to focus a task form
    isShowTaskForm && document.getElementById("taskForm").focus();
    return () => {
      console.log("List show task form Unmounting")
    }
  }, [isShowTaskForm]);


  const onSubmit = (data) => {
    console.log("onSubmit: " + data);
    console.log(watch("task"));
    const taskName = watch("task");
    console.log("New taskName: " + taskName);
    console.log("Pre task: " + task.task + ": " + task.userId + ": " + task.listId)
    console.log("rawId: " + getRawId(props.listId))
    console.log(taskName);
    const newTask = {
      "task": taskName,
      "userId": task.userId,
      "listId": getRawId(props.listId),
      "sequenceNum":props.tasks.length,
    };

    setTask(newTask);
    console.log("new task :");
    console.log(newTask);
    postTask(newTask);
  }

  const postTask = async (newTask) => {

    await postNewTask(newTask);
    props.setIsPosted(!props.isPosted);
    toggleIsShowTaskForm(false);
  }


  const updateListTitle =async  () =>{
    const editListName = watch("editListName");
    const listId = getRawId(props.listId)
    const newList = {

      "name": editListName,
      "boardId": props.boardId,
      "sequenceNum":props.list.sequenceNum,

    };


    updateList(listId,newList).then(res =>{
      props.doneEditListTitle(!props.editListTitle);
    });

    toggleTitleEdit(true);
  }

  const remakeLists = async (listData,listId) =>{
    await deleteList(getRawId(listId));
    props.setIsListDeleted(!props.isListDeleted);

    let sequenceNum;
    listData.forEach((obj,index) => {
      if(obj.id === listId) {
        sequenceNum = obj.sequenceNum;

      }
    })

    const begin = sequenceNum + 1;
    for(let i=begin ; i < props.listData.length; i++){
      let newList = {
        "name":listData[i].name,
        "boardId": listData[i].boardId,
        "sequenceNum":sequenceNum,
      };
      sequenceNum ++;
      console.log("listData[i] index :" + i);
      console.log(listData[i])
      updateList(getRawId(listData[i].id),newList);
    }

  }

  return (

    <Draggable
      draggableId={props.listId}
      index={props.index}
      key={props.listId}
    >
      {(provided, snapshot) => (
        <Container
          {...provided.draggableProps}
          {...provided.dragHandleProps}
          ref={provided.innerRef}
          isDragging={snapshot.isDragging}
          style={getItemStyle(
            snapshot.isDragging,
            provided.draggableProps.style
          )}
        ><div>

          {props.list.taskIds.length === 0
          ? <button onClick = {async () => {
              await remakeLists(props.listData,props.listId);
              props.setIsListDeleted(props.isListDeleted);
          }}　className="delete-list-button">

          <div id="delete-icon"><FontAwesomeIcon icon={['far', 'times-circle']} /></div></button>
          : null
          }

          {/* <div>{props.list.sequenceNum}</div>
          <div>{props.list.id}</div> */}

          <ClickAwayListener onClickAway={() => toggleTitleEdit(true)}>
          <Title onDoubleClick={()=>toggleTitleEdit(!isTitleEdit)}>
            {_.get("editListName.type",errors) === "required" && (<span className="list-title-error">入力必須です</span>)}
            {_.get("editListName.type",errors) === "maxLength" && (<span className="list-title-error">文字数30以下でお願いします</span>)}
            {isTitleEdit
            ? <div>{props.list.name}</div>
            : <form onSubmit={handleSubmit(updateListTitle)}>
              <input type="text" name="editListName" id="task-input-form"
              ref={register({required:true,maxLength: 30})}
              className="list-retitle" initialvalue={props.list.name}></input>
              </form>
          }</Title>
           </ClickAwayListener>

           <ClickAwayListener onClickAway={() => setTimeout(toggleIsShowTaskForm(false))}>
          {isShowTaskForm
            ? <><ToggleButton onClick={() => toggleIsShowTaskForm(false)}>-</ToggleButton>
            {_.get("task.type",errors) === "required" &&
            (<span className="new-task-validate">入力必須です</span>)}
            {_.get("task.type",errors) === "maxLength"
             && (<span className="new-task-validate">文字数30以下でお願いします</span>)}
              <form onSubmit={handleSubmit(onSubmit) } className="form-group">
                <input id="taskForm" name="task" type="text" initialvalue=""
                  ref={register({ required: true, maxLength: 30 })}
                   className="form-control"
                  //onChange={handleNameChange}
                ></input>

                <input type="hidden" value={props.listId}/>
                <input type="submit" value="送信" className="btn-outline-dark" id="new-task-form"/>
              </form></>
            : <ToggleButton onClick={() => toggleIsShowTaskForm(true)}>+</ToggleButton>
          }</ClickAwayListener>

          <Droppable droppableId={props.list.id} type="TASK">
            {(provided, snapshot) => (
              <TaskList
                ref={provided.innerRef}
                {...provided.droppableProps}
                isDraggingOver={snapshot.isDraggingOver}
              >
                {props.tasks.map((task, index) => (
                  <Task key={task.id} task={task} index={index}
                  tasks={props.tasks}

                   setIsDetailPosted={props.setIsDetailPosted}
                   isDetailPosted={props.isDetailPosted}
                   setIsDeleted={props.setIsDeleted}
                   isDeleted={props.isDeleted}
                  />
                ))}
                {provided.placeholder}
              </TaskList>
            )}
          </Droppable>
          </div>

        </Container>

      )}
    </Draggable>

  )

}
export default List;
