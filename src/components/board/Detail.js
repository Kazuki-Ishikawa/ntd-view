import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { useForm } from "react-hook-form";
import Popup from 'reactjs-popup';
import moment from 'moment'
import _ from "lodash/fp";
import { getRawId } from '../common/Util.js';
import DatePicker from 'react-modern-calendar-datepicker';

import "react-modern-calendar-datepicker/lib/DatePicker.css";
import '../../CSS/Form.css';
import '../../CSS/Detail.css';
import '../../CSS/GetApi.css';

const Detail = (props) => {

    const id = getRawId(props.task.id);

    const task = props.task;
    const [togglePut, setTogglePut] = useState(false);



    const [selectedDay, setSelectedDay] = useState(props.task.dueDateObj);

    const { register, handleSubmit, watch, errors } = useForm();


    const formatObjToStr =(dateObj)=>{
        if(selectedDay === null || selectedDay === "" || selectedDay === undefined){
            return null;
        }
        const dateStr = selectedDay.year +"-"+ selectedDay.month +"-"+ selectedDay.day;

        //let dateMoment = moment(dateStr);
        //dateMoment.format("YYYY-MM-DD");
        return new Date(dateStr);
    }
    const onSubmit = (data) => {
        console.log("onSubmit: " + data);
        console.log(watch("detail"));
        const editTask = watch("task");
        const detailName = watch("detail");
        //const dueDate = watch("dueDate");
        console.log("selectedDay" );
        console.log(selectedDay);
        const dueDate = formatObjToStr(selectedDay);
        console.log("New detailName: " + detailName);
        console.log("Pre detail: " + task.dueDate + ": " + task.completedDate);
        console.log(detailName);
        console.log("task.task :");
        console.log(task.task);
        /*
        const reDetailTask = {
            "task":task.task,
            "userId":task.userId,
            "listId":task.listId,
            "detail":detailName,
            "dueDate":dueDate,
            "completedDate":task.completedDate,
            "sequenceNum":task.sequenceNum
        };
        */
        const reDetailTask = {
            ...task,
            "task": editTask,
            "detail": detailName,
            "dueDate": dueDate,
        }
        setTogglePut(!togglePut);
        //setTask(reDetailTask);
        console.log("props: ");
        console.log(props);
        console.log("pre task");
        console.log(task);
        console.log("new task :");

        console.log(id);
        console.log(reDetailTask);
        console.log("aaa is" + aaa.format("YYYY-MM-DD"));
        putDetail(id, reDetailTask).then(res => {
            props.setIsDetailPosted(!props.isDetailPosted);
            // props.setEditedDueDate(!props.editedDueDate);
        })

        /* const fuga = async (id, reDetailTask) => {
             await putDetail(id, reDetailTask);
         }
         fuga(id, reDetailTask);
         props.setIsDetailPosted(!props.isDetailPosted)*/
        //formattedDueDate = task.dueDate.toLocaleDateString();
    }

    // const testTask = {
    //     "task":"xxxx",
    //     "userId":1,
    //     "listId":1,
    //     "detail":"rrrrr",
    //     "dueDate":"2020-09-09T01:25:39.055+00:00",
    //     "completedDate":"2020-09-09T01:25:39.055+00:00"
    // };

    const putDetail = (id, reDetailTask) => {
        return axios.put(process.env.REACT_APP_API_URI + "task/update/" + id, reDetailTask
        ).then((res) => {
            console.log("Post: " + res.data);
            console.log(reDetailTask);
            //props.setIsDetailPosted(!props.isDetailPosted);
        });
    }

    let dueDate = props.task.dueDate;
    let aaa = moment(dueDate);


    useEffect(() => {
        // fetchTaskInfo();
        return () => {
            console.log("Unmounting")
        }
    }
        , [togglePut]
    );


    // const detail = props.map((task,index) => {
    //     return(
    //         <div key = {index}className = "userInfo">
    //             <sapn>{task.detail}</sapn>
    //             <sapn>{task.dueDate}</sapn>
    //         </div>
    //     )
    // })


    // required="faise"　type="date" defaultValue={aaa.format("YYYY-MM-DD")}
    // ref={register} className="due-edit" required pattern="\d{4}-\d{2}-\d{2}"

    return (
        <>
            <Popup trigger={<button className="task-botton" id="task-detail-btn"> … </button>} position="right center" modal nested
                className="detail-popup">

                <div className="detail-contents">

                    {/* <div className="detail-cautions">
                <div>
                    { ((props.task.detail === null || props.task.detail === ""))
                        ? <div className="detail-caution">詳細を入力してください</div>
                        : null
                    }
                </div>
                <div>
                    { ((props.task.dueDate === null || props.task.dueDate === ""))
                        ? <div className="detail-caution">期限を入力してください</div>
                        : null
                    }
                </div>
            </div> */}

                    <form onSubmit={handleSubmit(onSubmit)} noValidate="novalidate" className="detail-forms">

                        {_.get("task.type", errors) === "required" &&
                            (<span className="edit-task-error">入力必須です</span>)}
                        {_.get("task.type", errors) === "maxLength"
                            && (<span className="edit-task-error">文字数30以下でお願いします</span>)}

                        <div className="edit-task">
                            <span className="edit-task-title">タスク変更</span>
                            <textarea name="task" type="text" defaultValue={props.task.task}
                                ref={register({ required: true, maxLength: 30 })} placeholder="" className="detail-edit"></textarea>
                            <label></label>
                            <span className="focus_line"><i></i></span>
                        </div>

                        <div className="detail-title">
                            <span className="detail-name">詳細</span>
                            <textarea name="detail" type="text" defaultValue={props.task.detail}
                                ref={register} placeholder="" className="detail-edit"></textarea>
                            <label></label>
                            <span className="focus_line"><i></i></span>
                        </div>

                        <div className="due-title">
                            <span >期限</span>
                            <label className="due-label"></label>

                            <DatePicker
                                value={selectedDay}
                                onChange={setSelectedDay}
                                inputPlaceholder="Select a day"
                                shouldHighlightWeekends //土日だけ赤にするコマンド
                                inputClassName="due-edit"
                            />
                        </div>



                <input type="submit" value="編集" className="submit-btn"
                            onSubmit={() => { alert('編集しました'); }} />
                    </form>
                </div>

            </Popup>
        </>
    )

}
export default Detail;

export const detailWindou = (props) => {
    return (
        <div><div>aaaa</div>
            { ((props.task.detail === null || props.task.detail === ""))
                ? <div>詳細を入力してください</div>
                : <div>{props.task.detail}</div>
            }
        </div>
    )
}
