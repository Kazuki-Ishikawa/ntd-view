import React from 'react';

import { Draggable } from 'react-beautiful-dnd'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import Detail from './Detail.js';
import { deleteTask } from '../apis/DeleteApi';
import { updateTask } from '../apis/PutApi.js';
import { getRawId } from '../common/Util';

import "../../CSS/Home.css";
import Container from "../style/StyledTask";

const Task = (props) => {

  const isDragDisabled = false;
  const remakeTasks = async (tasks, deletedId) => {
    await deleteTask(getRawId(deletedId));

    let sequenceNum;
    tasks.forEach((obj, index) => {
      if (obj.id === deletedId) {
        sequenceNum = obj.sequenceNum;

      }
    })

    const begin = sequenceNum + 1;
    for (let i = begin; i < tasks.length; i++) {
      let newTask = {
        "task": tasks[i].task,
        "listId": tasks[i].listId,
        "userId": tasks[i].userId,
        "detail": tasks[i].detail,
        "color": tasks[i].color,
        "dueDate": tasks[i].dueDate,
        "sequenceNum": sequenceNum
      };
      sequenceNum++
      updateTask(getRawId(tasks[i].id), newTask);
    }

    console.log(tasks);

  }

  return (
    <Draggable
      draggableId={props.task.id}
      index={props.index}
      isDragDisabled={isDragDisabled}
    >
      {(provided, snapshot) => (

        <Container
          {...provided.draggableProps}
          {...provided.dragHandleProps}
          ref={provided.innerRef}
          isDragging={snapshot.isDragging}
          isDragDisabled={isDragDisabled}
          labelColor={props.task.dueColor}
        >

          {/* <div>{props.task.sequenceNum} : {props.task.id}</div>*/}
          <div>{props.task.task}</div>

          {props.task.dueDate === null
            ? <input type="hidden" value="" />
            : <input type="hidden" value={props.task.dueDate} />
          }

          <Detail task={props.task} className="task-botton"
            setIsDetailPosted={props.setIsDetailPosted}
            isDetailPosted={props.isDetailPosted}
          />

          <button className="task-botton" onClick={async () => {
            //await deleteTask(getRawId(props.task.id));
            await remakeTasks(props.tasks, props.task.id);
            props.setIsDeleted(!props.isDeleted);
            alert('タスクを削除しました');
          }}
          >

            <FontAwesomeIcon icon={['fas', 'trash-alt']} />
          </button>
        </Container>
      )}
    </Draggable>
  )

}
export default Task;
