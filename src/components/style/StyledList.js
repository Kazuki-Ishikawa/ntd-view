import styled from 'styled-components';

/* CSS used by a styled component */
export const Container = styled.div`
margin: 8px;
border: 1px solid lightgrey;
border-radius: 2px;
width: 220px;

display: flex;
flex-direction: column;
backgroud:red;
`


export const Title = styled.h3`
  padding: 8px;
  font-family: 'Permanent Marker', cursive;
  font-size: 25px;
  text-align: center;
`;

export const TaskList = styled.div`
  padding: 8px;
  transition: background-color 0.2s ease;
  background-color: ${props =>
    props.isDraggingOver ? 'skyblue' : 'white'}
  flex-grow: 1;
  min-height: 100px;
`;

export const ToggleButton = styled.span`
width: 40px;
height: 40px;
padding:8px 14px;
border: 0px solid ;
font-size: 16px;
color: #ffffff;
font-size:bold;

background-color:#1bdb65;

-moz-border-radius: 5px;
-webkit-border-radius: 5px;
border-radius: 10px;
margin-bottom:50px;
box-shadow: 1px 1px 1px #3d3d3d;
table-layout: fixed;
&:hover {
  background-color:#01b446;
  cursor: default;
}
`



const grid = 8;
export const getItemStyle = (isDragging, draggableStyle) => ({

  userSelect: 'none',
  padding: grid * 2,
  margin: `0 ${grid}px 0 0`,
  display: 'block',
  width: '300px',
  // height: '400px',

  // change background colour if dragging
  background: isDragging ? '#a3ff9f' : '#a3ff9f',
  border: "solid 5px",
  "BorderColor": "black",
  "MozBorderRadius": "0px",
  "WebkitBorderRadius": "0px",
  "borderRadius": "10px",
  "position":"relative",
  "top":"0px",

  "boxShadow": isDragging
    ? '5px 5px 10px'
    : '1px 1px 1px',

  ...draggableStyle,
});
