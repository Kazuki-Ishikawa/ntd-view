import styled from 'styled-components';

const NORMAL = 1;
const CLOSE_DEADLINE = 2;
const OVERDUE = 3;

const Container = styled.div`
border: 1px solid lightgrey;
border-radius: 2px;
padding: 8px;
margin-bottom: 15px;
background: #f5f5f5;

border-left: ${props =>
    props.labelColor === NORMAL ? 'solid 20px #1bdb65'
      : props.labelColor === CLOSE_DEADLINE ? 'solid 20px #999999'
        : props.labelColor === OVERDUE ? 'solid 20px #db551b' : 'solid 20px #1bdb65'
  };

position: relative;
top: 20px;

box-shadow : ${props =>
    props.isDragging
      ? '5px 5px 10px'
      : '2px 2px 1px'};

transform: ${props =>
    props.isDragging
      ? 'rotate(0deg)'
      : 'rotate(0deg)'
  };

transition: background-color 0.2s ease;
background-color: ${props =>
    props.isDragDisabled
      ? '#f5f5f5'
      : props.isDragging
        ? '#f5f5f5'
        : '#f5f5f5'};
`
export default Container;