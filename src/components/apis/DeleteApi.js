import axios from 'axios';

export const deleteTask = async (id) => {
    const res = await axios.delete(process.env.REACT_APP_API_URI +"task/delete/" + id);
    console.log("Deleted!  res.data :");
    console.log(res.data);
    return; 
}

export const deleteList = async (id) => {

   const res = await axios.delete(process.env.REACT_APP_API_URI +"list/delete/" + id)
   console.log("Deleted!  res.data :");
   console.log(res.data);
    return;
 }