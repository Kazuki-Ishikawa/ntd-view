import axios from 'axios';

//emailをもとにDBからuser情報検索
export const fetchUserInfoByEmail = async (email) => {
    const res = await axios.get(process.env.REACT_APP_API_URI + "user/get/byEmail/" + email);
    console.log("fetch user by email :");
    console.log(res.data);
    return res.data;
}