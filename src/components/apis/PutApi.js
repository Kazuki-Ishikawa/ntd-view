import axios from 'axios';

export const updateList = (listId, newList) => {

  // const res = await axios.put(process.env.REACT_APP_API_URI +"list/update/" + listId, newList);
  // console.log("Update List Title ! ");
  // console.log(res);
  // return res;
  return axios.put(process.env.REACT_APP_API_URI + "list/update/" + listId, newList)
    .then(res => {
      console.log("Update List Title ! ");
      console.log(res);
    });

}

export const updateTask = (id, newTask) => {

  return axios.put(process.env.REACT_APP_API_URI + "task/update/" + id, newTask)
    .then(res => {
      console.log("Update Task !")
      console.log(res)
    });

}
