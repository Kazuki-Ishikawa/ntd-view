
import axios from 'axios';

//ページ飛ぶ前にセッションidの確保

export async function postNewUser(newUser) {
  // res には 従来のthen関数で得ていたresponseが入る
  const res = await axios.post(process.env.REACT_APP_API_URI +"user/set", newUser)
  const newUserId = res.data.id; // new user id which is created right now.

  const newBoard = {
    "name": "新しいボード",
    "userId": newUserId,
  };

  console.log("newBoard created!");
  console.log(newBoard);

  const boardRes = await axios.post(process.env.REACT_APP_API_URI +"board/set", newBoard)
  
  const newBoardId = boardRes.data.id;
  const newLists = [{
    "name": "To Do",
    "boardId": newBoardId,
    "sequenceNum":1,
  }, {
    "name": "In Progress",
    "boardId": newBoardId,
    "sequenceNum":2,
  }, {
    "name": "Done",
    "boardId": newBoardId,
    "sequenceNum":3,
  }];

  console.log("初期リスト：");
  console.log(newLists);

  newLists.forEach((newList, index) => {
    axios.post(process.env.REACT_APP_API_URI +"list/set", newList).then((res) => {
      console.log('Initial List Created! ' + index)
      console.log(res.data)
      // setIsListPosted(!isListPosted);
      
    });
  })

  return newUserId
}


export const updateTask = (id, newTask) => {

  axios.put(process.env.REACT_APP_API_URI + "task/update/" + id, newTask
  ).then((res) => {
    console.log("Put Called! ->res data: ");
    console.log(res.data);
    console.log(newTask);
  });
}

export async function getSocialLoginUser(accountType,email,snsId){
  email = accountType + email;//GOOG_ishiyama.tomoki@m.alhinc.jp
  snsId = accountType + snsId;//GOOG_1215484313543515231
  const res = await axios.get('http://localhost:8080/ntd/user/get/socialLogin/'+email+'/' + snsId);
  console.log("get social login user called! :");
  console.log(res);
 
  return res.data;
}

export async function postNewSocialLoginUser(accountType,newUser){
  newUser.snsId = accountType + newUser.snsId;
  newUser.email = accountType + newUser.email;
   ;
  const newUserId = await postNewUser(newUser);
  return newUserId;
}

