import axios from 'axios';

export const postNewList = async (newList) => {

    const res = await axios.post(process.env.REACT_APP_API_URI +"list/set", newList);
    console.log('Post new list :')
    console.log(res.data)
    return;
}

export const postNewTask = async (newTask) => {

    const res = await axios.post(process.env.REACT_APP_API_URI +"task/set", newTask);
    console.log('Post new task :')
    console.log(res.data)
    return;
}