import React from 'react';
import { GoogleLogin, GoogleLogout } from 'react-google-login';
// import FacebookLogin from 'react-facebook-login';
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { getSocialLoginUser,postNewSocialLoginUser } from "../apis/Api.js";
import { setSession } from './Util'

import "../../CSS/Signup.css";

const GOOGLE = "GOOG_";
const FACEBOOK = "FB_";

const OauthButtons = (props) => {

    const responseGoogle = async (response) => {
        console.log(response);

        if (response === null || response === "" || response === undefined || response.error !== undefined) {
            // return
        } else {

            //get user data from our data base to check if the user is alrady exist
            const user = await getSocialLoginUser(
                GOOGLE,
                response.profileObj.email,
                response.profileObj.googleId);

            console.log('get social login user in OauthButton: ');
            console.log(user);

            //when a social login user is not exist ,creat a new user data
            if (user === undefined || user === "" || user === null) {
                const newUser = {
                    "name": response.profileObj.name,
                    "snsId": response.profileObj.googleId,
                    "email": response.profileObj.email
                }
                const newUserId = await postNewSocialLoginUser(GOOGLE,newUser);
                setSession(newUserId)
                props.redirectToBoardWithTutorial();
            } else {//the user is already exist
                setSession(user.id);
                props.redirectToBoard();
            }
            //props.parrentProps.history.push('./board');
        }

    }


    //todo : Check if the FB user does not have email
    const responseFacebook = async (response) => {
        console.log(response);
        if (response === null || response === "" || response === undefined || response.error !== undefined) {
            // return
        } else {

            //get a user data from our data base to check if the user is alrady exist
            const user = await getSocialLoginUser(
                FACEBOOK,
                response.email,
                response.userID);

            console.log('get social login user in OauthButton: ');
            console.log(user);

            //when a social login user is not exist ,creat a new user data
            if (user === undefined || user === "" || user === null) {
                const newUser = {
                    "name": response.name,
                    "snsId": response.userID,
                    "email": response.email
                }
                const newUserId = await postNewSocialLoginUser(FACEBOOK,newUser);
                setSession(newUserId)
                props.redirectToBoardWithTutorial();
            } else {//the user is already exist
                setSession(user.id);
                props.redirectToBoard();
            }
            //props.parrentProps.history.push('./board');
        }

    }

    // const componentClicked = () => {
    //     console.log("Component clicked!!")
    // }

    return (
        <div className="wrapper-social-login">
            {/* <GoogleLogin
          clientId="184944951880-r61ddvtf8vr10jg0heg6877u2faq8so4.apps.googleusercontent.com"
          buttonText="Google Login"
          onSuccess={responseGoogle}
          onFailure={responseGoogle}
          cookiePolicy={'single_host_origin'}
        /> */}

            <GoogleLogin
                clientId="184944951880-r61ddvtf8vr10jg0heg6877u2faq8so4.apps.googleusercontent.com"
                render={renderProps => (
                    <button className="social-login" onClick={renderProps.onClick} disabled={renderProps.disabled} id="google">
                        <FontAwesomeIcon icon={['fab', 'google']} />
                    </button>
                )}
                buttonText="Login"
                onSuccess={responseGoogle}
                onFailure={responseGoogle}
                cookiePolicy={'single_host_origin'}
            />

            {/* <GoogleLogout
                clientId="184944951880-r61ddvtf8vr10jg0heg6877u2faq8so4.apps.googleusercontent.com"
                buttonText="Logout"
                onLogoutSuccess={responseGoogle}
            /> */}

            <FacebookLogin
                appId="378245496900934"
                autoLoad={false}
                callback={responseFacebook}
                fields="name,email,picture"
                render={renderProps => (
                    <button className="social-login" onClick={renderProps.onClick} disabled={renderProps.disabled}id="facebook">
                        <FontAwesomeIcon icon={['fab', 'facebook-square']} />
                    </button>
                )}
            />

            {/* <FacebookLogin
            appId="378245496900934"
            autoLoad={false}
            fields="name,email,picture"
            onClick={componentClicked}
            callback={responseFacebook}
        /> */}

            {/* <div>
                <div>{name}</div>
                <div>{email}</div>
                <div>{snsId}</div>
            </div> */}

            {/* <FacebookLogout
            appId="378245496900934"
            autoLoad={false}
            fields="name,email,picture"

            callback={responseFacebook}
        />   */}



        </div>
    )

}
export default OauthButtons;