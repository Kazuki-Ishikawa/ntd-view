import React from 'react';
import styled from 'styled-components';

const CopyRightWithStyle = styled.div`
width: 100%;
color: #333333;
background: rgba(0,0,0,0);
text-align: center;
position:absolute;
bottom: 0px;
`
const Copyright = () =>{
    return(

        <CopyRightWithStyle>Copyright © 2020 June Projects</CopyRightWithStyle>
 
    )
}
export default Copyright;