import moment from "moment";


 export const getRawId = (strId) =>{
    const rawId = parseInt(strId.replace(/^list-|task-/, ''));
    return rawId;
}

 export const setSession = (loginUserId) =>{
     sessionStorage.setItem('loginUserId', loginUserId);
 }

 export const getSession = () =>{
     return sessionStorage.getItem('loginUserId');
 }

export const logout = (redirectToLogin) => {
    sessionStorage.removeItem('loginUserId');
    redirectToLogin();
}

export const getNewSequenceNum = () => {

}



export const isCloseDeadline = (dueDate) => {
    const NORMAL = 1;
    const CLOSE_DEADLINE = 2;
    const OVERDUE = 3;
    //'solid 20px #1bdb65'
    // CSS Style in js file
    // It's possible to change some css propaty dinamicaly
    //現在時刻
    const now = new Date();
    const CLOSE_DEADLINE_DAY = 2;
    //const deadline = new Date(now.getFullYear(), now.getMonth(), now.getDate() - 2);
  // console.log("現在時刻" + now);

    //const dueDate = props.task.dueDate;
    const dateTypeDueDate = new Date(dueDate);

    //console.log("〆切：" + dueDate);
    //console.log("dateType  DueDate:  " + dateTypeDueDate)
    if (dueDate === null || dueDate === "" || dueDate === undefined) {
      return NORMAL;
    }

    if ((dateTypeDueDate - now) / 86400000 < -1) {
      return CLOSE_DEADLINE;

      //締め切り日　－　現在時刻  <= 2
    } else if ((dateTypeDueDate - now) / 86400000 === 0 || ((dateTypeDueDate - now) / 86400000 <= CLOSE_DEADLINE_DAY)) {
      return OVERDUE;
    }
  };



 export const formatStrToObj = (dueDateStr) =>{

    if(dueDateStr === null || dueDateStr === "" || dueDateStr === undefined){
        return null;
    }
    let dueDateMoment = moment(dueDateStr);

    const resultObj =  {
        year:parseInt(dueDateMoment.year()),
        month:parseInt(dueDateMoment.month())+1,
        day:parseInt(dueDateMoment.date()),
    }

    return resultObj;
}