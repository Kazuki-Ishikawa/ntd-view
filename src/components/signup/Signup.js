import React, { useState, useEffect } from 'react';
import { useForm } from "react-hook-form";

import sha512 from 'js-sha512';

import { getSession, setSession } from '../common/Util';
import OauthButtons from '../common/OauthButtons';
import Copyright from '../common/Copyright';
import { postNewUser } from "../apis/Api";
import {fetchUserInfoByEmail} from "../apis/GetApi";

import '../../CSS/Signup.css';
import "../../CSS/Login.css";

const Signup = (props) => {

  const { register, handleSubmit, watch } = useForm();

  const [nameInfo, setNameInfo] = useState({
    name: '',
    hasNameEmptyError: false,
    hasNameFormatError: false,
    hasNameExcessError: false
  });

  const [emailInfo, setEmailInfo] = useState({
    email: '',
    hasEmptyError: false,
    hasFormatError: false,
    hasSpaceError: false
  });

  const [passwordInfo, setPasswordInfo] = useState({
    password: '',
    hasPasswordEmptyError: false,
    hasPasswordFormatError: false,
    hasPasswordExcessError:false
  });

  const [confirmationInfo, setConfirmationInfo] = useState({
    confirmation: '',
    hasConfirmationError: false
  })

  const [signupErrorText, setSignupErrorText] = useState('');

  //各フォーム入力時のエラーメッセージの変数宣言
  let nameEmptyErrorText;
  let nameFormatErrorText;
  let nameExcessErrorText;
  let emailEmptyErrorText;
  let emailFormatErrorText;
  let emailSpaceErrorText;
  let passwordEmptyErrorText;
  let passwordFormatErrorText;
  let passwordExcessErrorText;
  let confirmationErrorText;


  useEffect(() => {
    console.log('mounting');
    console.log('名前空っぽのエラー　=' + nameInfo.hasNameEmptyError);
    console.log('名前フォーマットのエラー　=' + nameInfo.hasNameFormatError);
    console.log('名前書きすぎのエラー =' + nameInfo.hasNameExcessError);
    console.log('Eメール空っぽのえらー　=' + emailInfo.hasEmptyError);
    console.log('Eメールフォーマットのエラー =' + emailInfo.hasFormatError);
    console.log('Eメールフォーマットのエラー =' + emailInfo.hasSpaceError);
    console.log('ぱすわーど空っぽのえらー　=' + passwordInfo.hasPasswordEmptyError);
    console.log('ぱすわーどフォーマットのえらー　=' + passwordInfo.hasPasswordFormatError);
    console.log('ぱすわーど書きすぎのえらー　=' + passwordInfo.hasPasswordExcessError);
    console.log('確認用パスワードのエラー =' + confirmationInfo.hasConfirmationError);


  //いずれかのエラーがあった時、登録ボタンを非活性状態にする

  document.getElementById("normal-signup-button").disabled = true;

  if(nameInfo.hasNameEmptyError || nameInfo.hasNameFormatError || nameInfo.hasNameExcessError ||
    emailInfo.hasEmptyError || emailInfo.hasFormatError || emailInfo.hasSpaceError ||
    passwordInfo.hasPasswordEmptyError || passwordInfo.hasPasswordFormatError || passwordInfo.hasPasswordExcessError ||
    confirmationInfo.hasConfirmationError){
     document.getElementById("normal-signup-button").disabled = true;
    }else{
     document.getElementById("normal-signup-button").disabled = false;
    }

  });

  //nameフォームの変更を感知し、stateを更新
  const handleNameChange = (event) => {
    const inputValue = event.target.value;
    const isEmpty = inputValue === '';
    const isValid = (/\s/).test(inputValue);
    const isExcess = /^.{0,30}$/.test(inputValue);
    setNameInfo(
      {
        name: inputValue,
        hasNameEmptyError: isEmpty,
        hasNameFormatError: isValid,
        hasNameExcessError: !isExcess
      }
    );
  }
  //emailフォームに変更が加えられたとき、stateを更新
  const handleEmailChange = (event) => {
    const inputValue = event.target.value;
    const isEmpty = inputValue === '';
    const isValid = (/^[A-Za-z0-9]{1}[A-Za-z0-9_.-]*@{1}[A-Za-z0-9_.-]{1,}\.[A-Za-z0-9]{1,50}$/).test(inputValue);
    const isSpace =(/\s/).test(inputValue);
    console.log(!isValid);
    setEmailInfo(
      {
        email: inputValue,
        hasEmptyError: isEmpty,
        hasFormatError: !isValid,
        hasSpaceError: isSpace
      }
    );
  }

  //passwordフォームに変更が加えられたとき、stateを更新
  const handlePasswordChange = (event) => {
    const inputValue = event.target.value;

    // confirm password is not empty ,
    if (!(confirmationInfo.confirmation === null ||
      confirmationInfo.confirmation === undefined ||
      confirmationInfo.confirmation === "")) {
          const isDifferent = (inputValue !== confirmationInfo.confirmation);

          setConfirmationInfo({
              ...confirmationInfo,
              hasConfirmationError: isDifferent
          });
      }
      
    const isEmpty = inputValue === '';
    const isValid = /^(?=.*?[a-z])(?=.*?[A-Z])(?=.*?[0-9])/.test(inputValue);
    const isExcess = /^.{6,40}$/.test(inputValue);
    setPasswordInfo(
      {
        password: inputValue,
        hasPasswordEmptyError: isEmpty,
        hasPasswordFormatError: !isValid,
        hasPasswordExcessError: !isExcess
      }
    );
  }

  //confirmationフォームの変更を感知し、stateを更新
  const handleConfirmChange = (event) => {
    const inputValue = event.target.value;
    const isDifferent = (inputValue !== passwordInfo.password);
    setConfirmationInfo(
      {
        confirmation: inputValue,
        hasConfirmationError: isDifferent
      }
    );
  }

  //フォームが空だった場合、ErrorTextにメッセージを代入

  if (nameInfo.hasNameEmptyError) {
    nameEmptyErrorText = (
      <p className="error-message">名前を入力してください</p>
    );
    document.getElementById("normal-signup-button").disabled = true;
  }

  if (nameInfo.hasNameFormatError) {
    nameFormatErrorText = (
      <p className="error-message">スペースが入力されています</p>
    );
  }

  if (nameInfo.hasNameExcessError) {
    nameExcessErrorText = (
      <p className="error-message">名前は30字以下で記入してください</p>
    );
  }

  if (emailInfo.hasEmptyError) {
    emailEmptyErrorText = (
      <p className="error-message">メールアドレスを入力してください</p>
    );
  }

  if (emailInfo.hasFormatError) {
    emailFormatErrorText = (
      <p className="error-message">メールアドレスは@を含む半角英数字記号で入力してください</p>
    );
  }

  if (emailInfo.hasSpaceError) {
    emailSpaceErrorText = (
      <p className="error-message">スペースが入力されています</p>
    );
  }

  if (passwordInfo.hasPasswordEmptyError) {
    passwordEmptyErrorText = (
      <p className="error-message">パスワードを入力してください</p>
    );
  }

  if (passwordInfo.hasPasswordFormatError) {
    passwordExcessErrorText = (
      <p className="error-message">大文字小文字をそれぞれ含む半角英数字で設定してください</p>
    );
  }

  if (passwordInfo.hasPasswordExcessError) {
    passwordFormatErrorText = (
      <p className="error-message">6字以上40字以下で設定してください</p>
    );
  }

  if (confirmationInfo.hasConfirmationError) {
    confirmationErrorText = (
      <p className="error-message" id="error04">パスワードと確認用パスワードが一致しません</p>
    );
  }

  //↓登録ボタン押下後↓

  const onSubmit = () => {
    const email = watch("email");
    (async () => {
      const existingUser = await fetchUserInfoByEmail(email);
      signup(existingUser);
    })();
  }


  //新規登録処理
  const signup = async (existingUser) => {
    console.log(watch("userName") + " : " + watch("password") + " : " + watch("email"))

    const newUser = {
      "name": watch("userName"),
      "password": sha512(watch("password")),
      "email": watch("email")
    }
    console.log("new user:")
    console.log(newUser)
    console.log('【既存のユーザー】');
    console.log(existingUser);

    if (existingUser === null || existingUser === '') {
      console.log('人がいないので登録するぜ');

      const newUserId = await postNewUser(newUser);
      setSession(newUserId)
      console.log("登録したuserID" + getSession());


      props.history.push({
        pathname: './board',
        state: { isShowTutorial: true }
      })
    } else {
      console.log('人がいるので登録しないぜ');
      setSignupErrorText(<p>既に登録されているメールアドレスです</p>);
      console.log('【isSuccess:false】');
      console.log(signupErrorText);
    }
  }

  return (
    <>
      <div className="signup-back">

        <div className="fixed-header">
          <div className="header-items">
            <div className="header-icon">
              <a href="login" id="header-icon"><span id="fake">FAKE</span>Trello</a>
            </div>
          </div>
        </div>
        <div className="signup-forms">
          <form className="signup"
            onSubmit={handleSubmit(onSubmit)} >



            <p id="signup-hedder">signup to <span id="fake">FAKE</span> TRELLO</p>


            <div className="signup-form-wrapper">

              <div>
              <div className="error-message">{nameEmptyErrorText}</div>
              <div className="error-message">{nameFormatErrorText}</div>
              <div className="error-message">{nameExcessErrorText}</div>
              <input className="signup-form"
                type="name"
                placeholder="あなたのお名前"
                name="userName"
                ref={register}
                onChange={handleNameChange}
              />
              </div>



              <div>
              <div className="error-message">{emailEmptyErrorText}</div>
              <div className="error-message">{emailFormatErrorText}</div>
              <div className="error-message">{emailSpaceErrorText}</div>
              <div className="error-message">{signupErrorText}</div>
                <input className="signup-form"
                  placeholder="メールアドレス"
                  name="email"
                  ref={register}
                  onChange={handleEmailChange}
                />
              </div>

              <div>
              <div className="error-message">{passwordEmptyErrorText}</div>
              <div className="error-message">{passwordFormatErrorText}</div>
              <div className="error-message">{passwordExcessErrorText}</div>
                <input className="signup-form"
                  type="password"
                  placeholder="パスワード"
                  name="password"
                  ref={register}
                  onChange={handlePasswordChange}
                />
              </div>



              <div>
              <div className="error-message" >{confirmationErrorText}</div>
              <br/>
                <input className="signup-form"
                  type="password"
                  placeholder="確認用パスワード"
                  name="password"
                  ref={register}
                  onChange={handleConfirmChange}
                />
              </div>

            </div>

            <input type="submit" value="登録" id="normal-signup-button" className="signup-submit-botton"
            />
          </form>
          <div className="oauth-signup-wrapper">
            <OauthButtons
              redirectToBoardWithTutorial={() => props.history.push({
                pathname: './board',
                state: { isShowTutorial: true }
              })}
              redirectToBoard={() => props.history.push('./board')}
            />
          </div>
          <Copyright />
        </div>
      </div>

    </>
  )
}

export default Signup;