import React, { useState, useEffect } from 'react';
import { BrowserRouter, Route, Link } from 'react-router-dom';
import { withRouter } from 'react-router';
import { useForm } from "react-hook-form";
import axios from 'axios';
import sha512 from 'js-sha512';

import OauthButtons from '../common/OauthButtons';
import Copyright from '../common/Copyright';

import "../../CSS/Login.css";

const Login = (props) => {

  const { register, handleSubmit, watch, errors } = useForm();

  const [emailInfo, setEmailInfo] = useState({
    email: '',
    hasEmptyError: false,
    hasFormatError: false
  });

  const [passwordInfo, setPasswordInfo] = useState({
    password: '',
    hasPasswordEmptyError: false,
    hasPasswordFormatError: false,
    hasPasswordExcessError:false
  });

  const [loginErrorText, setLoginErrorText] = useState('');

  let emailEmptyErrorText;
  let emailFormatErrorText;
  let passwordEmptyErrorText;
  let passwordFormatErrorText;
  let passwordExcessErrorText;

  useEffect(() => {
    console.log('mounting');
    console.log('Eメール空っぽのえらー　=' + emailInfo.hasEmptyError);
    console.log('Eメールフォーマットのエラー =' + emailInfo.hasFormatError);
    console.log('ぱすわーど空っぽのえらー　=' + passwordInfo.hasPasswordEmptyError);
    console.log('ぱすわーどフォーマットのえらー　=' + passwordInfo.hasPasswordFormatError);
    console.log('ぱすわーど書きすぎのえらー　=' + passwordInfo.hasPasswordExcessError);

  //password入力時のエラーメッセージの変数宣言
  let passwordErrorText;
  //フォームが空だった場合、ErrorTextにメッセージを代入
  if (passwordInfo.hasPasswordError) {
    passwordErrorText = (
      <p className="errorMessage">パスワードを入力してください</p>
    );
  }

  // const onSubmit = () => {
  //   const email = watch("email");
  //   const password = sha512(watch("password"));
  //   fetchUserInfo(email, password);
  // }

    document.getElementById("login-button").disabled = true;
    if (emailInfo.hasEmailError || passwordInfo.hasPasswordError) {
      document.getElementById("login-button").disabled = true;
    } else {
      document.getElementById("login-button").disabled = false;
    }

    return () =>{
      console.log('unmounting');
    }
  });

//emailフォームに変更が加えられたとき、stateを更新
const handleEmailChange = (event) => {
  const inputValue = event.target.value;
  const isEmpty = inputValue === '';
  setEmailInfo(
    {
      email: inputValue,
      hasEmailError: isEmpty,
    }
  );
}

//passwordフォームに変更が加えられたとき、stateを更新
const handlePasswordChange = (event) => {
  const inputValue = event.target.value;
  const isEmpty = inputValue === '';
  setPasswordInfo(
    {
      password: inputValue,
      hasPasswordError: isEmpty,
    }
  );
}
//email入力時のエラーメッセージの変数宣言
let emailErrorText;
//フォームが空だった場合、Errortextにメッセージを代入
if (emailInfo.hasEmailError) {
  emailErrorText = (
    <p className="errorMessage" >メールアドレスを入力してください</p>
  );
}

//password入力時のエラーメッセージの変数宣言
let passwordErrorText;
//フォームが空だった場合、ErrorTextにメッセージを代入
if (passwordInfo.hasPasswordError) {
  passwordErrorText = (
    <p className="errorMessage">パスワードを入力してください</p>
  );
}




//↓ボタンが押されてからの処理↓
const onSubmit = () => {
  const email = watch("email");
  console.log(email);
  const password = sha512(watch("password"));
  console.log(password);
  fetchUserInfo(email, password);
}

const fetchUserInfo = (email, password) => {
  axios.get(process.env.REACT_APP_API_URI + "user/get/login/" + email + "/" + password).then((res) => {
    console.log(res);
    const userData = res.data;
    console.log("【user data】");
    console.log(userData);
    login(userData);
  });
}

const login = (userData) => {
  if (userData === null || userData === "") {
    props.history.push('/login');
    setLoginErrorText(
      <p>アカウントまたはパスワードが間違っています</p>
    );
  } else {
    sessionStorage.setItem('loginUserId', userData.id);
    const loginUserId = sessionStorage.getItem('loginUserId');
    console.log('ログインユーザーID = ' + loginUserId);
    props.history.push('./board');
  }
}

return (
  <div className="aaa">

<div className="fixed-header">
        <div className="header-items">
          <div className="header-icon">
            <a href="" id="header-icon"><span id="fake">FAKE</span>Trello</a>
          </div>

        </div>
      </div>

    <div className="normalLogin">
    <form onSubmit={handleSubmit(onSubmit)}>
      <div className="normal-login-form-contents">
        {/* <h1>{sessionStorage.getItem('loginUserId')}</h1> */}

        <p id="login-hedder">Log in to <span id="fake">FAKE</span> Trello</p>

        <div className="error-messages">
        {(props.location.state != undefined)
            ? <div className="error-message" id="login-require-message">{props.location.state.errorMessage}</div>
            : null
        }
        <span className="login-error-message">{loginErrorText}</span>
        </div>

        <div className="login-forms">
        <div >
          <div className="email-login-error">
        <span className="error-message">{emailErrorText}</span>
        </div>
          <label htmlFor="email" className="login-form-title">メールアドレス</label>
          <input id="email" className="normal-login-form" name="email"
            ref={register} onChange={handleEmailChange}
          />
        </div><br />

        <div>
        <div className="password-login-error">
        <span className="error-message">{passwordErrorText}</span>
        </div>
          <label htmlFor="password" className="login-form-title">パスワード　　</label>
          <input id="password" className="normal-login-form" name="password"
            type="password" ref={register} onChange={handlePasswordChange}
          />
        </div>
        </div>

        <div className="login-btn-wrapper">
          <button type="submit" id="login-button" className="btn"> LogIn</button><br />
        </div>
        <div className="oauth-login-wrapper">

        </div>



      </div>

    </form>
    <OauthButtons
             redirectToBoardWithTutorial={() => props.history.push({
              pathname: './board',
              state: { isShowTutorial: true }
            })}
            redirectToBoard={() => props.history.push('./board')}
    />
    <Link to="./signup" id="link-to-signup">SignUp</Link>
    <Copyright style={{position:'absolute'}} />
    </div>
  </div>
);
};



export default withRouter(Login);