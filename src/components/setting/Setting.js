import React, { useState, useEffect } from 'react';

import axios from 'axios';
import { useForm } from "react-hook-form";

import UpdatePassword from './Password.js';
import { logout } from '../common/Util';
import Copyright from '../common/Copyright';

import '../../CSS/Form.css';
import '../../CSS/Setting.css';

const Setting = (props) => {

 // const [userInfo, setUserInfo] = useState([]);
  //クエリパラメータの取得
  const id = props.match.params.id;
  const loginUserId = sessionStorage.getItem('loginUserId');

  //state, form の宣言、定義
  const [isUpdated, setIsUpdated] = useState(false);
  const { register, handleSubmit, watch/*, errors*/ } = useForm();
  const [isPasswordUpdated, setIsPasswordUpdated] = useState(false);

  let [user, setUser] = useState({
    id: 0,
    account: "",
    name: "",
    password: "",
    email: "",
    snsId: "",
    introduction: ""
  });

  const [nameInfo, setNameInfo] = useState({
    name: '',
    hasNameEmptyError: false,
    hasNameFormatError: false,
    hasNameExcessError: false
  });

  const [emailInfo, setEmailInfo] = useState({
    email: '',
    hasEmptyError: false,
    hasFormatError: false,
    hasSpaceError:false
  });

  const [settingErrorText, setSettingErrorText] = useState('');

  let nameEmptyErrorText;
  let nameFormatErrorText;
  let nameExcessErrorText;
  let emailEmptyErrorText;
  let emailFormatErrorText;
  let emailSpaceErrorText;
  useEffect(() => {

    fetchUserInfo();

    return () => {
      console.log("Unmounting")
    }
  }, [isUpdated, isPasswordUpdated]);

  useEffect(() => {
    console.log('mounting');
    console.log('名前空っぽのエラー　=' + nameInfo.hasNameEmptyError);
    console.log('名前フォーマットのエラー　=' + nameInfo.hasNameFormatError);
    console.log('名前書きすぎのエラー =' + nameInfo.hasNameExcessError);
    console.log('Eメール空っぽのえらー　=' + emailInfo.hasEmptyError);
    console.log('Eメールフォーマットのエラー =' + emailInfo.hasFormatError);
    console.log('Eメールスペースのエラー =' + emailInfo.hasSpaceError);

    //いずれかのエラーがあった時、登録ボタンを非活性状態にするド汚ねえ処理

    if (nameInfo.hasNameEmptyError || nameInfo.hasNameFormatError || nameInfo.hasNameExcessError ||
      emailInfo.hasEmptyError || emailInfo.hasFormatError || emailInfo.hasSpaceError) {
      document.getElementById("setting-submit-button").disabled = true;
    } else {
      document.getElementById("setting-submit-button").disabled = false;
    }

    return () => {
      console.log("Unmounting")
    }

  })


  //nameフォームの変更を感知し、stateを更新
  const handleNameChange = (event) => {
    const inputValue = event.target.value;
    const isEmpty = inputValue === '';
    const isValid = (/\s/).test(inputValue);
    const isExcess = /^.{0,30}$/.test(inputValue);
    setNameInfo(
      {
        name: inputValue,
        hasNameEmptyError: isEmpty,
        hasNameFormatError: isValid,
        hasNameExcessError: !isExcess
      }
    );
  }
  //emailフォームに変更が加えられたとき、stateを更新
  const handleEmailChange = (event) => {
    const inputValue = event.target.value;
    const isEmpty = inputValue === '';
    const isValid = (/^[A-Za-z0-9]{1}[A-Za-z0-9_.-]*@{1}[A-Za-z0-9_.-]{1,}\.[A-Za-z0-9]{1,50}$/).test(inputValue);
    const isSpace = (/\s/).test(inputValue);
    console.log(!isValid);
    setEmailInfo(
      {
        email: inputValue,
        hasEmptyError: isEmpty,
        hasFormatError: !isValid,
        hasSpaceError: isSpace
      }
    );
  }

  //各種エラーの際にエラーメッセージを代入する
  if (nameInfo.hasNameEmptyError) {
    nameEmptyErrorText = (
      <p className="error-message">名前を入力してください</p>
    );
  }

  if (nameInfo.hasNameFormatError) {
    nameFormatErrorText = (
      <p className="error-message">スペースが入力されています</p>
    );
  }

  if (nameInfo.hasNameExcessError) {
    nameExcessErrorText = (
      <p className="error-message">名前は30字以下で記入してください</p>
    );
  }

  if (emailInfo.hasEmptyError) {
    emailEmptyErrorText = (
      <p className="error-message">メールアドレスを入力してください</p>
    );
  }

  if (emailInfo.hasFormatError) {
    emailFormatErrorText = (
      <p className="error-message">メールアドレスは@を含む半角英数字記号で入力してください</p>
    );
  }

  if (emailInfo.hasSpaceError) {
    emailSpaceErrorText = (
      <p className="error-message">スペースが入力されています</p>
    );
  }


  //onSubmitで呼ばれる関数たち
  const onSubmit = (data) => {
    console.log("onSubmit: " + data);
    const name = watch("name");
    const account = watch("account");
    const email = watch("email");
    const introduction = watch("introduction");
    let updatedUser;
    if (user.snsId === '' || user.snsId === null) {
      updatedUser = {
        "id": id,
        "account": account,
        "name": name,
        "email": email,
        "password": user.password,
        "introduction": introduction
      };
    } else {
      updatedUser = {
        "id": id,
        "account": account,
        "name": name,
        "snsId": user.snsId,
        "email": user.email,
        "password": user.password,
        "introduction": introduction
      };
    }

    setUser(updatedUser);
    console.log("updated user :");
    console.log(updatedUser);
    updateUser(updatedUser);
    setIsUpdated(!isUpdated);
    // props.history.push("./board");
  }



  const fetchUserInfo = () => {
    axios.get(process.env.REACT_APP_API_URI + "user/get/" + loginUserId).then((res) => {
      console.log(res);
      const data = res.data;
      console.log("data:");
      console.log(data);
     // setUserInfo(res);
      setUser(data);
    });
  }

  const updateUser = (updatedUser) => {
    axios.put(process.env.REACT_APP_API_URI + "user/update/" + loginUserId, updatedUser).then((res) => {
      console.log("Put: " + res.data);
    });
  }


  return (
    <React.Fragment>

      <div className="setting-back">

        <div className="fixed-header">
          <div className="header-items">
            <div className="header-icon">
              <a href="board" id="header-icon"><span id="fake">FAKE</span>Trello</a>
            </div>

            <div className="header-item">
              <span onClick={() => logout(() => props.history.push('./login'))}
                test={"test"} className="header-text">log out</span>
            </div>
            <div className="header-item">
              <a href="Setting" className="header-text">setting</a>
            </div>

          </div>
        </div>

        <h1 id="setting-user">卍{loginUserId}卍</h1>
        <div className="setting">
          <form onSubmit={handleSubmit(onSubmit)} >

            <div className="setting-hedder">ユーザー情報編集</div>

            <div className="setting-contents">

              <div className="error-messages">
                <div className="error-message">{settingErrorText}</div>
                <div className="setting-error-message" id="name-setting-error">{nameEmptyErrorText}</div>
                <div className="setting-error-message" id="name-setting-error">{nameFormatErrorText}</div>
                <div className="setting-error-message" id="name-setting-error">{nameExcessErrorText}</div>
                <div className="setting-error-message" id="email-setting-error">{emailEmptyErrorText}</div>
                <div className="setting-error-message" id="email-setting-error">{emailFormatErrorText}</div>
                <div className="setting-error-message" id="email-setting-error">{emailSpaceErrorText}</div>

              </div>

              {/* <label htmlFor="account" className="setting-title">　　　 アカウント</label>
              <input className="setting-form"
                id="account" name="account"
                type="account"
                defaultValue={user.account}

                ref={register}
              /><br/> */}

              <div className="name-setting-wrapper">
                <label htmlFor="name" className="setting-title">　　　　　　名前</label>
                <input className="setting-form"
                  id="name" name="name"
                  type="name"
                  defaultValue={user.name}
                  ref={register}
                  onChange={handleNameChange}
                /><br /></div>

              {(user.snsId === '' || user.snsId === null) &&
                <React.Fragment>
                  <div className="email-setting-wrapper">
                    <label htmlFor="email" className="setting-title">　メールアドレス</label>
                    <input className="setting-form"
                      id="email" name="email"

                      defaultValue={user.email}
                      ref={register}
                      onChange={handleEmailChange}
                    /><br /></div>
                </React.Fragment>
              }
              {(user.snsId !== '') &&
                <input type="hidden" id="email" name="email" value={user.email} />
              }

              <label htmlFor="introduction" className="setting-title" id="label-introduction">        自己紹介</label>
              <textarea className="setting-form"
                id="introduction" name="introduction"
                type="text" cols="30" rows="5"
                defaultValue={user.introduction}
                ref={register}
              /><br />

              {(user.snsId === '' || user.snsId === null) &&
                <>
                  <label className="setting-title" id="password-setting-title">         パスワード</label>
                  <input type="hidden" id="password" name="password" value={user.password} /><br />
                </>
              }




              {/* <p>{user.password}</p> */}


              <div className="btn-wrapper">
                <input type="submit" id="setting-submit-button" value="更新！" className="btn setting-btn"
                  onClick={() => { alert("ユーザー情報を変更しました") }} />
              </div>
            </div>

          </form>
          <Copyright />
          {(user.snsId === '' || user.snsId === null) &&
            <UpdatePassword
              name={user.name}
              account={user.account}
              email={user.email}
              introduction={user.introduction}
              password={''}
              isPasswordUpdated={isPasswordUpdated}
              setIsPasswordUpdated={setIsPasswordUpdated}
            />}

        </div>
      </div>

    </React.Fragment>

  )
}

export default Setting;