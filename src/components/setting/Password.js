import React, { useState, useEffect } from 'react';
//import _ from "lodash/fp";
import { withRouter } from 'react-router-dom';
import axios from 'axios';
import { useForm } from "react-hook-form";
import Popup from 'reactjs-popup';
import sha512 from 'js-sha512';

import '../../CSS/Setting.css';
import '../../CSS/Form.css';

const UpdatePassword = (props) => {

    const { register, handleSubmit, watch/*, errors */ } = useForm();

    const [passwordInfo, setPasswordInfo] = useState({
        password: '',
        hasPasswordEmptyError: false,
        hasPasswordFormatError: false,
        hasPasswordExcessError: false
    });

    const [confirmationInfo, setConfirmationInfo] = useState({
        confirmation: '',
        hasConfirmationError: false,
    });

    const loginUserId = sessionStorage.getItem('loginUserId');

    const inputedConfirmPassword = watch("password");

    let passwordEmptyErrorText;
    let passwordFormatErrorText;
    let passwordExcessErrorText;
    let confirmationErrorText;

    useEffect(() => {
        if (document.getElementById("password-submit-button") === null ||
            document.getElementById("password-submit-button") === "" ||
            document.getElementById("password-submit-button") === undefined) {
            return;
        }

        console.log('mounting');
        console.log('ぱすわーど空っぽのえらー　=' + passwordInfo.hasPasswordEmptyError);
        console.log('ぱすわーどフォーマットのえらー　=' + passwordInfo.hasPasswordFormatError);
        console.log('ぱすわーど書きすぎのえらー　=' + passwordInfo.hasPasswordExcessError);
        console.log('確認用パスワードのエラー =' + confirmationInfo.hasConfirmationError);




        if (passwordInfo.hasPasswordEmptyError || passwordInfo.hasPasswordFormatError || passwordInfo.hasPasswordExcessError ||
            confirmationInfo.hasConfirmationError) {
            document.getElementById("password-submit-button").disabled = true;
        } else {
            document.getElementById("password-submit-button").disabled = false;
        }

        return () => {
            console.log("Unmounting")
        }
    });


    //passwordフォームに変更が加えられたとき、stateを更新
    const handlePasswordChange = (event) => {

        const inputValue = event.target.value;

        // confirm password is not empty ,
        if (!(confirmationInfo.confirmation === null ||
            confirmationInfo.confirmation === undefined ||
            confirmationInfo.confirmation === "")) {
                const isDifferent = (inputValue !== confirmationInfo.confirmation);

                setConfirmationInfo({
                    ...confirmationInfo,
                    hasConfirmationError: isDifferent
                });
            }

        const isEmpty = inputValue === '';
        const isValid = /^(?=.*?[a-z])(?=.*?[A-Z])(?=.*?[0-9])/.test(inputValue);
        const isExcess = /^.{6,40}$/.test(inputValue);
        setPasswordInfo(
            {
                password: inputValue,
                hasPasswordEmptyError: isEmpty,
                hasPasswordFormatError: !isValid,
                hasPasswordExcessError: !isExcess
            }
        );
    }

    //confirmationフォームの変更を感知し、stateを更新
    const handleConfirmChange = (event) => {
        const inputValue = event.target.value;
        const isDifferent = (inputValue !== passwordInfo.password);
        setConfirmationInfo(
            {
                confirmation: inputValue,
                hasConfirmationError: isDifferent
            }
        );
    }

    if (passwordInfo.hasPasswordEmptyError) {
        passwordEmptyErrorText = (
            <p className="error-message">パスワードを入力してください</p>
        );
    }

    if (passwordInfo.hasPasswordFormatError) {
        passwordExcessErrorText = (
            <p className="error-message">大文字小文字をそれぞれ含む半角英数字で設定してください</p>
        )
    }

    if (passwordInfo.hasPasswordExcessError) {
        passwordFormatErrorText = (
            <p className="error-message">6字以上40字以下で設定してください</p>
        )
    }

    if (confirmationInfo.hasConfirmationError) {
        confirmationErrorText = (
            <p className="error-message" id="error04">パスワードと確認用パスワードが一致しません</p>
        );
    }

    const onSubmit = (data, close) => {

        const inputConfirmPassword = confirmationInfo.confirmation;
        if (inputConfirmPassword === "" ||
            inputConfirmPassword === null ||
            inputConfirmPassword === undefined ||
            inputConfirmPassword !== passwordInfo.password) {

            setConfirmationInfo({
                ...confirmationInfo,
                hasConfirmationError: true
            });
            document.getElementById("password-submit-button").disabled = true;
            return;
        }

        console.log("onSubmit: " + data);
        const password = sha512(watch("password"));
        const updatedPassword = {
            "account": props.account,
            "name": props.name,
            "email": props.email,
            "introduction": props.introduction,
            "password": password
        };


        console.log("updated password :");
        console.log(updatedPassword);
        putPassword(loginUserId, updatedPassword);



        // window.close();
        console.log("props")
        console.log(props);
        props.history.push('./setting');


    }

    const putPassword = (loginUserId, updatedPassword) => {
        console.log("入力されたパスワード　=>");
        console.log(updatedPassword);
        axios.put(process.env.REACT_APP_API_URI + "user/update/" + loginUserId, updatedPassword
        ).then((res) => {
            console.log("Post:");
            console.log(res.data);
            console.log(updatedPassword);
            props.setIsPasswordUpdated(!props.isPasswordUpdated);
            alert("パスワードを変更しました");
        });
    }



    return (
        <React.Fragment>


            <Popup trigger={<button className="setting-password"> 変更 </button>} position="right center" modal nested >
                {close => (

                    <div className="setting-popup-contents">

                        <div className="modal-dialog" >
                            <div className="modal-content">
                            </div>
                        </div>
                        <div className="password-form-content">
                            <form onSubmit={handleSubmit(onSubmit)} noValidate="novalidate" className="password-form-group">


                                <div className="error-messages">
                                    <div className="password-error-message" id="password-setting-error">{passwordEmptyErrorText}</div>
                                    <div className="password-error-message" id="password-setting-error">{passwordFormatErrorText}</div>
                                    <div className="password-error-message" id="password-setting-error">{passwordExcessErrorText}</div>
                                    <div className="password-error-message" id="confirm-setting-error">{confirmationErrorText}</div>
                                </div>

                                <label htmlFor="password" className="setting-title-password">　　　　　パスワード</label>
                                <input className="setting-form"
                                    id="password-form" name="password"
                                    type="password"
                                    ref={register}
                                    onChange={handlePasswordChange}
                                /><br />

                                <label htmlFor="confirm" className="setting-title-password">　　確認用パスワード</label>
                                <input className="setting-form"
                                    id="confirm-password" name="confirm"
                                    type="password"
                                    ref={register}
                                    onChange={handleConfirmChange}
                                /><br />



                                <input type="submit" id="password-submit-button" value="保存" className="btn"
                                    onClick={(event) => {
                                        if (inputedConfirmPassword === "") {
                                            event.stopPropagation();
                                            alert("パスワードを入力してください");
                                        }

                                    }}
                                />
                            </form>
                            {/* <button className="btn "onClick={() => close()}>close</button> */}
                        </div>
                    </div>
                )}
            </Popup>

        </React.Fragment>
    )

}

export default withRouter(UpdatePassword);
